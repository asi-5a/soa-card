
const roomReducer = (state = { newselectedCard: {}, selectedCard: {}, display: false, gameReady: false, gameInfo: {} }, action) => {

    switch (action.type) {
        case "CARD_CHOOSE_TO_PLAY":
            state.selectedCard = action.card;
            const newselectedCard = { ...state.selectedCard };
            //console.log(newselectedCard);
            return { newselectedCard: newselectedCard };
        case "DISPLAY_PROGRESS_BAR":
            return { newselectedCard: state.newselectedCard, selectedCard: state.selectedCard, display: true }
        case "BEGIN_GAME":
            state.gameInfo = action.game;
            const newGame = { ...state.gameInfo };
            console.log(newGame);
            return { gameInfo: newGame, display: false, gameReady: true };
        default:
            return state;
    }
};

export default roomReducer;
