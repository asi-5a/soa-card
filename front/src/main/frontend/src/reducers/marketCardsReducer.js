
const marketCardsReducer = (state = { cards: [] }, action) => {

    switch (action.type) {
        case "MARKET_CARDS":
            state.cards = action.cards;
            const newCards = [...state.cards];
            return { cards: newCards };
        default:
            return state;
    }
};

export default marketCardsReducer;
