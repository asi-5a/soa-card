
let user = JSON.parse(localStorage.getItem("user"));
const userNullable = { user: null };
if (user == null) {
    user = userNullable;
}
const userListReducer = (state = { user }, action) => {
    switch (action.type) {
        case "LOG_IN":
            state.user = action.user;
            const newUserIn = { ...state.user };
            localStorage.setItem("user", JSON.stringify(action.user));
            return { user: newUserIn };
        case "LOG_OUT":
            localStorage.removeItem("user");
            state.user = userNullable;
            const oldUser = { ...state.user };
            return { user: oldUser };
        case "BUY_CARD":
            let newUser = { ...state.user };
            newUser.idCards.push(action.card.id);
            newUser.account -= action.card.price;
            localStorage.setItem('user', JSON.stringify(newUser));
            return { user: newUser };
        case "SELL_CARD":
            console.log('sell card');
            let newUserSell = { ...state.user };
            const idx = newUserSell.idCards.findIndex(c => c === action.card.id);
            if (idx > -1) {
                newUserSell.idCards.splice(idx, 1);
                newUserSell.account += action.card.price;
            }
            localStorage.setItem('user', JSON.stringify(newUserSell));
            return { user: newUserSell };
        default:
            return state;
    }
};

export default userListReducer;
