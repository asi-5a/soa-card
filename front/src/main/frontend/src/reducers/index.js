import { combineReducers } from 'redux';
import selectedCardReducers from './selectedCardReducers';
import userReducers from './userReducers';
import chatReducer from "./chatReducers";
import roomReducer from './roomReducers';
import marketCardsReducer from "./marketCardsReducer";

/*
reducer that can contains set of reducer, usefull when several reducers are used at a timeS
*/
const globalReducer = combineReducers({
    selectedCardReducers: selectedCardReducers,
    userReducers: userReducers,
    chatReducer: chatReducer,
    roomReducers: roomReducer,
    marketCardsReducer: marketCardsReducer
});
export default globalReducer;
