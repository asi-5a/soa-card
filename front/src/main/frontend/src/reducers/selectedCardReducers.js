

const selectedCardListReducer = (state = { selectedCard: {} }, action) => {
    switch (action.type) {
        case "CARD_SELECTED":
            state.selectedCard = action.card;
            const newselectedCard = { ...state.selectedCard };
            return { selectedCard: newselectedCard };
        default:
            return state;
    }
};

export default selectedCardListReducer;
