
const chatReducer = (state = { listMessages: {} }, action) => {
    if (action.type === "MSG_RECEIVED") {
        if (!state.listMessages[action.message.chatId]) {
            state.listMessages[action.message.chatId] = [];
        }
        if (action.message.message !== null) {
            state.listMessages[action.message.chatId].push(action.message);
        }
        const newListMessages = { ...state.listMessages };
        return { listMessages: newListMessages };
    } else {
        return state;
    }
};

export default chatReducer;
