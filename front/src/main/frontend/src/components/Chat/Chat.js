import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import './Chat.css';
import Message from "./Message/Message.js"

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import ChatService from "../../services/ChatService";
import {connect} from "react-redux";
import {messageReceived} from "../../actions";
import UserService from "../../services/UserService";

class Chat extends Component {

    chatService = new ChatService();
    userService = new UserService();

    offlineUsers = [];

    constructor(props) {
        super(props);
        this.handleSendMessage = this.handleSendMessage.bind(this);
        this.handleNewMessage = this.handleNewMessage.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.saveChat = this.saveChat.bind(this);
        this.state = {connectedUsers: [], userSelected: '', offlineUsers: []};
    }

    componentDidMount() {
        if (this.props.user === undefined || !this.props.user.login) return;
        this.chatService.subNewClient((error, userList) => {
            // userList = userList.filter(u => u.username !== this.props.user.login);
            this.setState({connectedUsers: userList});
            this.getOfflineUsers();
        });
        // this.chatService.connectChat(this.props.user.login.toString());
        this.chatService.subMessage(this.handleNewMessage);
        this.getOfflineUsers();
    }

    getOfflineUsers(connectedUsers) {
        if (!connectedUsers) connectedUsers = this.state.connectedUsers;
        this.userService.getAllUsers().then(data => {
            data.data = data.data.filter(u => {
                const idx = connectedUsers.findIndex(uc => uc.username === u.login);
                return idx === -1 && u.login !== this.props.user.login;
            });
            this.setState({offlineUsers: data.data});
        });
    }

    saveChat() {
        // let index = this.state.userSelected;
        Object.keys(this.props.listMessages).forEach(userSocketId => {
            const body = {
                "topic" : "CHAT",
                "from" : "CHATNODE",
                "intent" : "SAVE_MESSAGE",
                "message" : {
                    "senderId" : JSON.parse(this.userService.getUserLoggedIn()).id,
                    "receiverId" : this.state.userSelected,
                    "messages" : JSON.stringify(this.props.listMessages[userSocketId])
                }
            };
            this.chatService.saveChat(body);
        })
    }

    handleNewMessage(message) {
        const now = new Date();
        this.props.dispatch(messageReceived({
            date: `${now.getHours()}:${now.getMinutes()}`,
            pseudo: message.pseudo,
            message: message.message,
            chatId: message.senderSocketId
        }));
    }

    handleSendMessage() {
        const message = document.querySelector('#text-message').value;
        const id = this.state.userSelected;
        const now = new Date();
        this.chatService.sendMessage({
            id,
            message
        });
        this.props.dispatch(messageReceived({
            date: `${now.getHours()}:${now.getMinutes()}`,
            pseudo: this.props.user.login,
            message: message,
            chatId: this.state.userSelected
        }))
    }

    handleChange(event) {
        this.props.dispatch(messageReceived({
            date: null,
            pseudo: this.props.user.login,
            message: null,
            chatId: event.target.value
        }));
        this.setState({userSelected: event.target.value})
    }

    render() {
        if (this.props.user === undefined || !this.props.user.login) return (<h1>Please log in to use chat</h1>);
        return (
            <div className="chatContainer">
                <div className="topBar">
                    <label>Chat</label>
                    <label>{this.props.user.login}</label>
                    <AccountCircleIcon/>
                </div>
                <FormControl className="selectUser">
                    <InputLabel id="demo-simple-select-label">Connected users</InputLabel>
                    <Select value={this.state.userSelected} onChange={this.handleChange} id={"userToSendTo"}
                            labelId="demo-simple-select-label">
                        {this.state.connectedUsers.map(u => u.username !== this.props.user.login ?
                            <MenuItem key={Math.random()} value={u.id}>{u.username}</MenuItem> : null)}
                        {this.state.offlineUsers.map(u => u.username !== this.props.user.login ?
                            <MenuItem key={Math.random()} disabled value={''}>{u.login}</MenuItem> : null)}
                    </Select>
                </FormControl>
                <div className="listMsg">
                    {this.state.userSelected.length > 0 ? this.props.listMessages[this.state.userSelected].map((msg) =>
                        <Message key={Math.random()} message={msg}/>) : null}
                </div>
                <TextareaAutosize id={'text-message'} className="txtAreaMsg" aria-label="minimum height"
                                  rows={3} placeholder="Minimum 3 rows"/>
                <Button disabled={this.state.userSelected.length === 0} onClick={this.handleSendMessage}
                        className="btnSendMsg" variant="contained">
                    Send Msg
                    <ArrowForwardIcon/>
                </Button>
                <Button onClick={this.saveChat}>Save chat</Button>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        listMessages: state.chatReducer.listMessages,
        user: state.userReducers.user
    }
};

export default connect(mapStateToProps)(Chat);