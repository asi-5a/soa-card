import React, { Component } from 'react';
import './Message.css';
import Badge from '@material-ui/core/Badge';


class Message extends Component {

    render() {

        return (
            <div className="MsgBox">
                <Badge badgeContent={this.props.message.pseudo} color="primary" />
                <label className="time">{this.props.message.date}</label>
                <label className="msg">{this.props.message.message}</label>
            </div>
        );
    }
}

export default Message;