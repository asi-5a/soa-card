import * as React from "react";
import Box from '@material-ui/core/Box';
import { connect } from 'react-redux';
import "./Home.css"
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import VideogameAssetIcon from '@material-ui/icons/VideogameAsset';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import ChatIcon from '@material-ui/icons/Chat';
import {Link as RouterLink} from "react-router-dom";


class Home extends React.Component {
    render() {
        return (
            <div>             
                <Box className="boxContainer">
                    <Box component={RouterLink} to="/market/buy" className="BigItemCard" >
                        <ShoppingCartIcon/>
                        Buy
                    </Box>
                    <Box component={RouterLink} to="/room" className="BigItemCard">
                        <VideogameAssetIcon />
                        Play
                    </Box>
                    <Box component={RouterLink} to="/market/sell" className="BigItemCard" >
                        <LocalAtmIcon/>
                        Sell
                    </Box>
                    <Box component={RouterLink} to="/chat" className="BigItemCard">
                        <ChatIcon/>Chat
                    </Box>
                </Box>
            </div>
        )
    }
}

export default connect()(Home);
