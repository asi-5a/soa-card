import React, { Component } from 'react';
import './User.css';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import LinearProgress from '@material-ui/core/LinearProgress';
import CardShort from '../../card/CardShort';

class User extends Component {

    // eslint-disable-next-line
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className="userLine">
                <div className="gameBlock">
                    <AccountCircleIcon />
                    <label>{this.props.user.username}</label>
                    <LinearProgress className="progressBarrActionPoint" variant="determinate" value={50} />
                    <label>Action Points</label>
                </div>
                <CardShort className="cardUser" card={this.props.user.card}></CardShort>
            </div>


        );
    }
}

export default User;