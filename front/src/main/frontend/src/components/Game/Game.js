import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import './Game.css';
import User from './User/User.js'

import { connect } from "react-redux";
import EjectIcon from '@material-ui/icons/Eject';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import UserService from '../../services/UserService'

class Game extends Component {

    userService = new UserService();

    // eslint-disable-next-line
    constructor(props) {
        super(props);
        this.renderAlertPlayerWhoBegin = this.renderAlertPlayerWhoBegin.bind(this);
        this.renderPlayerEnemy = this.renderPlayerEnemy.bind(this);
    }

    renderAlertPlayerWhoBegin() {
        let firstRound = this.props.gameInfo.firstRound;
        if (typeof firstRound !== "undefined") {
            alert("Le joueur " + this.props.gameInfo['j' + firstRound].username + " commence");
        }
    }

    renderPlayerEnemy() {
        let me, enemy;
        if (this.props.gameInfo.j1.username === JSON.parse(this.userService.getUserLoggedIn()).login) {
            me = this.props.gameInfo.j1;
            enemy = this.props.gameInfo.j2;
        } else {
            me = this.props.gameInfo.j2;
            enemy = this.props.gameInfo.j1;
        }
        return (
            <div className="gameUserBlock">
                <User user={me}></User>
                <div className="separation">
                    <div></div>
                    <label>VS</label>
                    <div></div>
                </div>
                <User user={enemy}></User>
            </div>
        );
    }

    render() {
        this.renderAlertPlayerWhoBegin();
        const renderUsers = this.renderPlayerEnemy();
        return (
            <div className="gameContainer">
                {renderUsers}
                <div className="gameButton">
                    <Button variant="contained" color="primary">
                        <EjectIcon />
                        Attack
                    </Button>
                    <Button className="btnSendMsg" variant="contained" color="primary">
                        <ExitToAppIcon />
                        End Turn
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        gameInfo: state.roomReducers.gameInfo
    }
};

export default connect(mapStateToProps)(Game);