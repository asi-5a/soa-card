import * as React from "react";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import CardLine from "../../card/CardLine";
import './TableCartes.css';
import {connect} from "react-redux";
// import {connect} from "react-redux";


class TableCartes extends React.Component {

    cards = require('../../../mocks/cards');

    // eslint-disable-next-line
    constructor(props) {
        super(props);
    }



    render() {
        return (
            <Paper className={'root'}>
                <Table className={'table'} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell align="center">Name</TableCell>
                            <TableCell align="center">Family</TableCell>
                            <TableCell align="center">Affinity</TableCell>
                            <TableCell align="center">Energy</TableCell>
                            <TableCell align="center">HP</TableCell>
                            <TableCell align="center">Attack</TableCell>
                            <TableCell align="center">Defense</TableCell>
                            <TableCell align="center">Price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.cards ? this.props.cards.map(row => (
                            <CardLine key={row.id} card={row} />
                        )) : null}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        cards: state.marketCardsReducer.cards
    }
};

export default connect(mapStateToProps)(TableCartes);
