import * as React from "react";
import {connect} from "react-redux";
import TableCartes from "./table/TableCartes";
import './Market.css';
import CardFull from "../card/CardFull";
import CardService from "../../services/CardService";
// import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Buy from "../market-action/buy/Buy";
import Sell from "../market-action/sell/Sell";
import UserService from "../../services/UserService";
import {newCardsMarket} from "../../actions";


class Market extends React.Component {

    // eslint-disable-next-line
    cardService = new CardService();
    userService = new UserService();

    constructor(props) {
        super(props);
        this.state = {cards: []};
        if (this.props.match.params.action === 'buy') {
            this.cardService.getCardsToSell().then((response) => {
                this.props.dispatch(newCardsMarket(response.data));
            }, err => {
                console.error('error fetching cards', err);
            });
        } else {
            let user = this.userService.getUserLoggedIn();
            if (user && user.length > 0) {
                user = JSON.parse(user);
                this.getUsersCard(user).then(cards => {
                    console.log('cards', cards);
                    cards = cards.filter(c => typeof c === 'object');
                    this.props.dispatch(newCardsMarket(cards));
                    //this.setState({cards});
                });
            }
        }
    }

    getUsersCard(user) {
        const qTab = [];
        user.idCards.forEach(id => {
            qTab.push(new Promise((resolve, reject) => {
                this.cardService.getCardById(id).then(res => {
                    resolve(res.data);
                }, err => {
                    console.error('error', err);
                    reject(err);
                })
            }))
        });
        return Promise.all(qTab);
    }

    render() {
        const action = this.props.match.params.action;
        const actionButton = action && action === 'buy' ? (<Buy card={this.props.selectedCard}/>) :
            <Sell card={this.props.selectedCard}/>;
        return (
            <div className={'container'}>
                <div className={'table-cartes'}>
                    <TableCartes />
                </div>
                <div className={'selected-card'}>
                    <CardFull card={this.props.selectedCard}/>
                    <div className={'action'}>
                        {actionButton}
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        selectedCard: state.selectedCardReducers.selectedCard
    }
};

export default connect(mapStateToProps)(Market);
