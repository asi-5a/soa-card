import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Redirect } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import HomeIcon from '@material-ui/icons/Home';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import { logOut } from '../../actions';

function useStyles() {
    return makeStyles(theme => ({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }));
}

class G2Toolbar extends React.Component {
    constructor(props) {
        super(props);

        this.renderConnectionPart = this.renderConnectionPart.bind(this);
        this.handleDisconnection = this.handleDisconnection.bind(this);
        this.setRedirect = this.setRedirect.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }
    state = {
        redirect: false
    };

    setRedirect() {
        this.setState({
            redirect: true
        })
    }

    renderRedirect() {
        if (this.state.redirect) {
            return <Redirect to='/signin' />
        }
    }
    
    handleDisconnection() {
        this.props.dispatch(
            logOut()
        );
        // this.props.history.push('/signin');
        this.setRedirect();
    }

    renderConnectionPart() {
        if (typeof this.props.userlogin !== 'undefined') {
            return (
                <Typography variant="h5">
                    <Button variant="contained" color="secondary" onClick={this.handleDisconnection}>Disconnect</Button>
                    <AccountCircleIcon /> {this.props.userlogin}
                </Typography>
            )
        } else {
            return (
                <Button variant="contained" color="secondary" component={Link} to="/signin">Login</Button>
            )
        }
    }

    render() {
        const classes = useStyles();
        let connexionPart = this.renderConnectionPart();
        let renderRedirect = this.renderRedirect();

        return (
            <AppBar position="static">
                {renderRedirect}
                <Toolbar>
                    <Grid
                        justify="space-between" // Add it here :)
                        container
                    >
                        <Grid item>
                            <div>
                                <Typography variant="h5">
                                    <Button variant="contained" component={Link} to="/home" startIcon={<HomeIcon />}>Home</Button>
                                    <AccountBalanceWalletIcon />
                                    {this.props.userAccount} €
                                </Typography>
                            </div>
                        </Grid>
                        <Grid item>
                            <Typography variant="h6" className={classes.title}>
                                {this.props.title}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <div>
                                {connexionPart}
                            </div>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        )
    }
}
//link data from the store to local props
const mapStateToProps = (state, ownProps) => {
    return {
        userlogin: state.userReducers.user.login,
        userAccount: state.userReducers.user.account
    }
};

export default connect(mapStateToProps)(G2Toolbar);