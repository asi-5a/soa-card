import * as React from "react";
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {cardSelected, newCardsMarket, sellCard} from "../../../actions";
import UserService from "../../../services/UserService";
import MarketService from "../../../services/MarketService";
import CardService from "../../../services/CardService";

class Sell extends React.Component {

    userService = new UserService();
    marketService = new MarketService();
    cardService = new CardService();

    constructor(props, context) {
        super(props, context);
        this.handleClick = this.handleClick.bind(this);
    }


    handleClick() {
        let user = this.userService.getUserLoggedIn();
        if (user && user.length > 0) {
            user = JSON.parse(user);
            this.marketService.sellCard({
                user_id: user.id,
                card_id: this.props.card.id
            }).then(() => {
                this.props.dispatch(sellCard(this.props.card));
                this.cardService.getUsersCard(JSON.parse(this.userService.getUserLoggedIn())).then(cards => {
                    cards = cards.filter(c => typeof c === 'object');
                    this.props.dispatch(newCardsMarket(cards));
                    this.props.dispatch(cardSelected(null));
                });
            });
        }
    }

    render() {
        if (this.props.card.cardReference === undefined) {
            return null
        }
        return (
            <Button variant="contained" color="primary" onClick={this.handleClick}>Sell</Button>
        )
    }

}

export default connect()(Sell);