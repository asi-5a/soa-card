import * as React from "react";
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {buyCard, cardSelected, newCardsMarket} from "../../../actions";
import UserService from "../../../services/UserService";
import MarketService from "../../../services/MarketService";
import CardService from "../../../services/CardService";

class Buy extends React.Component {

    userService = new UserService();
    marketService = new MarketService();
    cardService = new CardService();

    constructor(props, context) {
        super(props, context);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        let user = this.userService.getUserLoggedIn();
        if (user && user.length > 0) {
            user = JSON.parse(user);
            const buyOrder = {
                user_id: user.id,
                card_id: this.props.card.id
            };
            this.marketService.buyCard(buyOrder).then(res => {
                this.cardService.getCardsToSell().then(res => {
                    console.log('res', res);
                    res.data = res.data.filter(c => c.id !== this.props.card.id);
                    this.props.dispatch(newCardsMarket(res.data));
                    this.props.dispatch(buyCard(this.props.card));
                    this.props.dispatch(cardSelected(null));
                });
            }, err => {
                console.error('err', err);
            })
        }
    }

    render() {
        if (this.props.card.cardReference === undefined) {
            return null
        }
        return (
            <Button variant="contained" color="primary" onClick={this.handleClick}>Buy</Button>
        )
    }

}

export default connect()(Buy);