import React, { Component } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import './CardLine.css';
import {connect} from "react-redux";
import {cardSelected} from '../../actions'

class CardLine extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.dispatch(cardSelected(this.props.card))
    }

    render() {

        return (
            <TableRow onClick={this.handleClick} className={'table-row'}>
                <TableCell align="center">
                    <img alt={this.props.card.cardReference.name + ' icon'} src={this.props.card.cardReference.smallImgUrl} />
                </TableCell>
                <TableCell align="center">
                    {this.props.card.cardReference.name}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.cardReference.family}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.cardReference.affinity}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.energy}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.hp}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.attack}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.defence}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.price} €
                </TableCell>
            </TableRow>
        );
    }
}



export default connect()(CardLine);
