import React, { Component } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import './CardLine.css';
import { connect } from "react-redux";
import { Button } from '@material-ui/core';

import { cardChooseToPlay } from "../../actions";
import { showWaitingBar } from "../../actions";

import PlayService from "../../services/PlayService";

class CardLineRoom extends Component {

    playService = new PlayService();

    constructor(props) {
        super(props);
        this.SendCardToNode = this.SendCardToNode.bind(this);
    }

    SendCardToNode() {
        this.props.dispatch(cardChooseToPlay(this.props.card));
        this.props.dispatch(showWaitingBar(this.props.display));
        this.handleSendCard();

    }

    handleSendCard() {
        const card = this.props.card;
        console.log('sending: ', card);
        this.playService.sendCard(card);
    }

    render() {

        return (
            <TableRow onClick={this.handleClick} className={'table-row'}>
                <TableCell align="center">
                    <img alt={this.props.card.cardReference.name + ' icon'} src={this.props.card.cardReference.smallImgUrl} />
                </TableCell>
                <TableCell align="center">
                    {this.props.card.cardReference.name}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.cardReference.family}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.energy}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.hp}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.attack}
                </TableCell>
                <TableCell align="center">
                    {this.props.card.defence}
                </TableCell>
                <TableCell align="center">
                    <Button color="primary" onClick={this.SendCardToNode}>
                        select
                    </Button>
                </TableCell>
            </TableRow>
        );
    }
}



export default connect()(CardLineRoom);
