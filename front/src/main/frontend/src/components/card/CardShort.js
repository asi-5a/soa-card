import React, { Component } from 'react';
import './CardShort.css';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Badge from '@material-ui/core/Badge';
import Typography from "@material-ui/core/Typography";

class CardShort extends Component {

    // eslint-disable-next-line
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Card className={"cardShort"}>
                <CardActionArea>
                    <CardContent className="lineInfoCardShort">
                        <Badge badgeContent={this.props.card.attack} color="secondary" showZero="true"></Badge>
                        <Typography gutterBottom variant="h5" component="h2">{this.props.card.cardReference.name}</Typography>
                        <Badge badgeContent={this.props.card.energy} color="primary" showZero="true"></Badge>
                    </CardContent>
                    <CardMedia
                        className={'media'}
                        image={this.props.card.cardReference.smallImgUrl}
                        title="Card img"
                    />
                </CardActionArea>
            </Card>

        );
    }
}

export default CardShort;