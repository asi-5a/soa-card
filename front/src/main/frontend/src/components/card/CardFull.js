
import React, { Component } from 'react';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import SecurityIcon from '@material-ui/icons/Security';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocalDiningIcon from '@material-ui/icons/LocalDining';
import './CardFull.css';
// import card from  '../../mocks/card';


class CardFull extends Component {

    // eslint-disable-next-line
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.card.cardReference === undefined) {
            return null
        }
        return (
            <Card className={'card'}>
                <CardActionArea>
                    <CardMedia
                        className={'media'}
                        image={this.props.card.cardReference.imgUrl}
                        title="Card img"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.card.cardReference.name}: {this.props.card.price} €
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {this.props.card.cardReference.description}
                        </Typography>
                        <div className={'stats'}>
                            <div className={'stats-row'}>
                                <div><FavoriteIcon /> {this.props.card.hp}</div>
                                <div><LocalDiningIcon /> {this.props.card.attack}</div>
                            </div>
                            <div className={'stats-row'}>
                                <div><SecurityIcon /> {this.props.card.defence}</div>
                                <div><DirectionsRunIcon /> {this.props.card.energy}</div>
                            </div>
                        </div>
                    </CardContent>
                </CardActionArea>
            </Card>
        );
    }
}



export default CardFull;
