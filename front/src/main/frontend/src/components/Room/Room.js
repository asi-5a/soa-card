import React, { Component } from 'react';

import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import CardLineRoom from "../card/CardLineRoom";
import CircularProgress from '@material-ui/core/CircularProgress';
import { Redirect } from 'react-router-dom';

import './Room.css';

import { connect } from "react-redux";
import PlayService from "../../services/PlayService";
import UserService from '../../services/UserService';
import CardService from '../../services/CardService';


class Room extends Component {

    playService = new PlayService();
    userService = new UserService();
    cardService = new CardService();

    constructor(props) {
        super(props);
        this.state = {
            cards: []
        };
        this.handleNewGame = this.handleNewGame.bind(this);
        this.renderGameRedirect = this.renderGameRedirect.bind(this);
        let user = this.userService.getUserLoggedIn();
        if (user && user.length > 0) {
            user = JSON.parse(user);
            this.getUsersCard(user).then(cards => {
                cards = cards.filter(c => typeof c === 'object');
                this.setState({ cards: cards });
            });
        }
    }

    getUsersCard(user) {
        const qTab = [];
        user.idCards.forEach(id => {
            qTab.push(new Promise((resolve, reject) => {
                this.cardService.getCardById(id).then(res => {
                    resolve(res.data);
                }, err => {
                    console.error('error', err);
                    reject(err);
                })
            }))
        });
        return Promise.all(qTab);
    }

    componentDidMount() {
        this.playService.subGame(this.handleNewGame)
    }

    handleNewGame(Game) {
        console.log('message received', Game);
    }

    renderGameRedirect() {
        if (this.props.gameReady) {
            return (
                <Redirect to="/game"/>
            );
        }
    }

    render() {
        const cards = this.state.cards;
        const renderBeginGame = this.renderGameRedirect();
        return (
            <div className="roomContainer">
                <Paper className={'root'}>
                    { renderBeginGame }
                    <Table className={'table'} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell align="center">Name</TableCell>
                                <TableCell align="center">Family</TableCell>
                                <TableCell align="center">Energy</TableCell>
                                <TableCell align="center">HP</TableCell>
                                <TableCell align="center">Attack</TableCell>
                                <TableCell align="center">Defense</TableCell>
                                <TableCell align="center">Select</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cards.map(row => (
                                <CardLineRoom variant="contained" color="primary" key={row.id} card={row} />
                            ))}
                        </TableBody>
                    </Table>
                </Paper>

                <div className="waintingDiv">
                    {this.props.display === true ?
                        <div className="waintingDiv"><CircularProgress /> Recherche de joueur en cours ... </div> : ""}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        newselectedCard: state.roomReducers.newselectedCard,
        display: state.roomReducers.display,
        gameReady: state.roomReducers.gameReady
    }
};

export default connect(mapStateToProps)(Room);