export const buyCard = (card) => {
    return { type: 'BUY_CARD', card: card };
};

export const sellCard = (card) => {
    return { type: 'SELL_CARD', card: card };
};

export const cardSelected = (card) => {
    return { type: 'CARD_SELECTED', card: card };
};

export const cardChooseToPlay = (card) => {
    return { type: 'CARD_CHOOSE_TO_PLAY', card: card };
};

export const showWaitingBar = (display) => {
    return { type: 'DISPLAY_PROGRESS_BAR', display: display };
};

export const beginGame = (gameArg) => {
    return { type: 'BEGIN_GAME', game: gameArg };
};

export const logIn = (userLogin) => {
    return { type: 'LOG_IN', user: userLogin };
};

export const logOut = () => {
    return { type: 'LOG_OUT' };
};

export const messageReceived = (message) => {
    return { type: 'MSG_RECEIVED', message };
};

export const newCardsMarket = (cards) => {
    return { type: 'MARKET_CARDS', cards };
};
