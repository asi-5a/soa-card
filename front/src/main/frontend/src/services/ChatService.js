import SocketService from "./SocketService";
const axios = require('axios');

class ChatService {

    socketService = null;
    apiUrl = 'http://localhost:9001';

    constructor() {
        this.socketService = new SocketService();
    }

    async connectChat(pseudo) {
        await this.socketService.q;
        this.socketService.socket.emit('nouveau_client', pseudo);
    }

    async subNewClient(cb) {
        await this.socketService.q;
        this.socketService.socket.on('nouveau_client', pseudo => cb(null, pseudo));
    }

    async subMessage(cb) {
        await this.socketService.q;
        this.socketService.socket.on('message', (message, error) => {
            cb(message);
        });
    }

    async sendMessage(message) {
        await this.socketService.q;
        this.socketService.socket.emit('message', message);
    }

    saveChat(payload) {
        return axios.post(this.apiUrl + '/save_chat', payload);
    }

}

export default ChatService;