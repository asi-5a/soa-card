import SocketService from "./SocketService";
import store from '../store';
import {beginGame} from '../actions/index';

class PlayService {
    socketService = null;

    constructor() {
        this.socketService = new SocketService();
    }

    async sendCard(card) {
        await this.socketService.q;
        console.log('cardSend');
        console.log(JSON.stringify(card));
        this.socketService.socket.emit('matchmaking',JSON.stringify(card));
    }

    async subGame(Game) {
        await this.socketService.q;
        console.log('subbing to game reception');
        this.socketService.socket.on('NewGame', (gameInfo, error) => {
            store.dispatch(beginGame(gameInfo));
            console.log('game', gameInfo);
        });
    }
}

export default PlayService;