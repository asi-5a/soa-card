import openSocket from 'socket.io-client';
import UserService from "./UserService";
var socket = null;

class SocketService {
    userService = new UserService();
    resQ = () => { };
    socket=null;
    q = new Promise(resolve => this.resQ = resolve);

    constructor() {
        if (socket === null) {
            socket = openSocket('http://localhost:9001?token=' + JSON.parse(this.userService.getUserLoggedIn()).login);
        }
        this.socket = socket;
        this.resQ();
    }
}

export default SocketService;