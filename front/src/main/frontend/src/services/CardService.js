const axios = require('axios');

class CardService {
    baseUrl = 'http://127.0.0.1:8080/soacard-service';

    getCardsToSell() {
        return axios.get(this.baseUrl + '/cards_to_sell');
    }

    getCardById(id) {
        return axios.get(this.baseUrl + '/card/' + id);
    }

    getUsersCard(user) {
        const qTab = [];
        user.idCards.forEach(id => {
            qTab.push(new Promise((resolve, reject) => {
                this.getCardById(id).then(res => {
                    resolve(res.data);
                }, err => {
                    console.error('error', err);
                    reject(err);
                })
            }))
        });
        return Promise.all(qTab);
    }
}

export default CardService;
