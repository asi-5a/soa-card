const axios = require('axios');

class UserService {

    baseUrl = 'http://127.0.0.1:8080/soauser-service';

    login(login, password) {
        return axios.get(this.baseUrl + `/auth?login=${login}&pwd=${password}`);
    }

    getUserLoggedIn() {
        return localStorage.getItem('user');
    }

    signUp(payload) {
        return axios.post(this.baseUrl + '/user', payload);
    }

    getAllUsers() {
        return axios.get(this.baseUrl + '/users');
    }
}

export default UserService;
