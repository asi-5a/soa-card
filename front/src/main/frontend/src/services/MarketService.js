const axios = require('axios');

class MarketService {
    baseUrl = 'http://127.0.0.1:8080/soastore-service';
    //baseUrl = 'http://127.0.0.1:8083';

    buyCard(storeOrder) {
        return axios.post(this.baseUrl + '/buy', storeOrder);
    }

    sellCard(storeOrder) {
        return axios.post(this.baseUrl + '/sell', storeOrder);
    }
}

export default MarketService;