import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from './components/home/Home';
import SignIn from './components/sign/SignIn';
import SignUp from './components/sign/SignUp';
import Market from './components/market/Market';
import G2Toolbar from './components/toolbar/G2Toolbar'

//import needed to use redux with react.js
// import { createStore } from 'redux';
// import myReducers from './reducers'
import store from './store';
import { Provider } from 'react-redux';
import Chat from "./components/Chat/Chat";
import Game from './components/Game/Game';
import Room from './components/Room/Room';

// const store = createStore(myReducers);

const routing = (
    <Provider store={store} >
        <Router>
            <div>
                <G2Toolbar />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/home" component={Home} />
                    <Route path="/game" component={Game} />
                    <Route path="/room" component={Room} />
                    <Route path="/market/:action" component={Market} />
                    <Route path="/signin" component={SignIn} />
                    <Route path="/signup" component={SignUp} />
                    <Route path="/chat" component={Chat} />
                </Switch>
            </div>
        </Router>
    </Provider>
);

// source for routing : https://codeburst.io/getting-started-with-react-router-5c978f70df91


ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
