var stompit = require('stompit');
var activemq = {};

var envActiveMq = process.env.hostActiveMq || '127.0.0.1';

var connectOptions = {
    'host': envActiveMq,
    'port': 61613,
    'connectHeaders':{
        'host': '/',
        'login': 'soacard',
        'passcode': 'soapassword',
        'heart-beat': '5000,5000'
    }
};

var sendHeaders = {
    'destination': '/topic/CHAT',
    'content-type': 'text/plain'
};

activemq.sendMessage = (message) => {
    stompit.connect(connectOptions, function(error, client) {

        if (error) {
            console.log('connect error ' + error.message);
            return;
        }

        var frame = client.send(sendHeaders);
        frame.write(JSON.stringify(message));
        frame.end();

        client.disconnect();
    });
};

module.exports = activemq;
