package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreUserDto implements Serializable {

    private Integer idUser;

    private Integer idCard;

    private float priceCard;

}
