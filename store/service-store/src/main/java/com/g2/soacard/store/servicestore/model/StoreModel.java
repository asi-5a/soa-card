package com.g2.soacard.store.servicestore.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class StoreModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;

	@OneToMany
    private Set<StoreCardId> storeCardIds = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<StoreCardId> getIdCardsList() {
		return storeCardIds;
	}

	public void setIdCardsList(Set<StoreCardId> storeCardIds) {
		this.storeCardIds = storeCardIds;
	}

	public void addCard(StoreCardId storeCardId) {
		this.storeCardIds.add(storeCardId);
	}
	

}
