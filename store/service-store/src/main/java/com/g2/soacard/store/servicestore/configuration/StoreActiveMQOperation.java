package com.g2.soacard.store.servicestore.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.configuration.activeMQ.GeneralActiveMQOperation;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import com.g2.soacard.store.servicestore.service.StoreService;
import model.CardIntent;
import model.view.CardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class StoreActiveMQOperation extends GeneralActiveMQOperation {

    private final StoreService storeService;

    @Autowired
    public StoreActiveMQOperation(@Lazy StoreService storeService) {
        this.storeService = storeService;
    }

    @Override
    protected void receiveMessage(Letter letter) {
        LinkService from = letter.getFrom();
        switch (from){
            case CARD:
                CardIntent cardIntent = mapper.convertValue(letter.getIntent(), new TypeReference<CardIntent>(){});
                if(cardIntent == CardIntent.CARDS_GENERATED){
                    CardView cardView = mapper.convertValue(letter.getMessage(), new TypeReference<CardView>(){});
                    storeService.addCardsToStore(cardView);
                }
                break;
        }

    }

}
