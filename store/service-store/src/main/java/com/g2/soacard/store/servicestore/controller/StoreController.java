package com.g2.soacard.store.servicestore.controller;

import com.g2.soacard.store.servicestore.model.StoreOrder;
import com.g2.soacard.store.servicestore.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class StoreController {

    @Autowired
    private StoreService storeService;

    @RequestMapping(method= RequestMethod.POST,value="/buy")
    private boolean getAllCards(@RequestBody StoreOrder order) {
        return storeService.buyCard(order.getUser_id(),order.getCard_id());

    }

    @RequestMapping(method=RequestMethod.POST,value="/sell")
    private boolean getCard(@RequestBody StoreOrder order) {
        return storeService.sellCard(order.getUser_id(),order.getCard_id());
    }

}
