package com.g2.soacard.store.servicestore.service;

import model.view.CardView;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface StoreService {

    void generateNewStore(String name, int nb);

    void addCardsToStore(CardView cardView);

    boolean buyCard(Integer user_id, Integer card_id);

    boolean sellCard(Integer user_id, Integer card_id);

    Set<Integer> getAllStoreCard();

}
