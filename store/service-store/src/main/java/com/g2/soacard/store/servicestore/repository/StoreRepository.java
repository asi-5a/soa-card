package com.g2.soacard.store.servicestore.repository;

import com.g2.soacard.store.servicestore.model.StoreModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StoreRepository extends CrudRepository<StoreModel, Integer> {

}
