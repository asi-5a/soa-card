package com.g2.soacard.store.servicestore.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import com.g2.soacard.store.servicestore.configuration.StoreActiveMQOperation;
import com.g2.soacard.store.servicestore.model.StoreCardId;
import com.g2.soacard.store.servicestore.model.StoreModel;
import com.g2.soacard.store.servicestore.repository.StoreRepository;
import com.g2.soacard.store.servicestore.service.StoreService;
import lombok.extern.slf4j.Slf4j;
import model.*;
import model.view.CardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class StoreServiceImpl implements StoreService {

    private final StoreActiveMQOperation storeActiveMQOperation;

    private final StoreRepository storeRepository;

    private StoreModel store;

    @Autowired
    public StoreServiceImpl(StoreRepository storeRepository, StoreActiveMQOperation storeActiveMQOperation) {
        this.storeActiveMQOperation = storeActiveMQOperation;
        this.storeRepository = storeRepository;
    }

    @Override
    public void generateNewStore(String name, int nb) {
        StoreModel store =new StoreModel();
        store.setName(name);
        store = storeRepository.save(store);
        this.store=store;

        Letter letter = new Letter();
        letter.setTopic(LinkService.CARD);
        letter.setIntent(CardIntent.CARDS_GENERATED);
        letter.setMessage(new StoreCardDto(5, store.getId()));
        storeActiveMQOperation.sendMessage(letter);
        log.info("A new store ({}) has been generated", store.getName());
    }

    @Override
    public void addCardsToStore(CardView cardView) {
        Optional<StoreModel> store = storeRepository.findById(cardView.getIdStore());
        if(store.isPresent()){
            List<Integer> idCards = cardView.getIdCards();
            for(Integer idCard : idCards){
                StoreCardId storeCardId = new StoreCardId();
                storeCardId.setCardId(idCard);
                store.get().addCard(storeCardId);
            }
            log.info("Cards ({}) have been added to store {}", store.get().getIdCardsList().toString(), store.get().getName());
        }
    }

    public boolean buyCard(Integer user_id, Integer card_id) {
        Letter letter = new Letter();
        letter.setFrom(LinkService.STORE);
        letter.setTopic(LinkService.CARD);
        letter.setIntent(StoreIntent.BUY_CARD);
        letter.setMessage(new StoreUserDto(user_id, card_id, 0f));
        storeActiveMQOperation.sendMessage(letter);
        log.debug("User {} has initialized the action of buying card {}", user_id, card_id);
        return true;
    }

    public boolean sellCard(Integer user_id, Integer card_id) {
        Letter letter = new Letter();
        letter.setFrom(LinkService.STORE);
        letter.setTopic(LinkService.CARD);
        letter.setIntent(StoreIntent.SELL_CARD);
        letter.setMessage(new StoreUserDto(user_id, card_id, 0f));
        storeActiveMQOperation.sendMessage(letter);
        log.debug("User {} has initialized the action of selling card {}", user_id, card_id);
        return true;
    }

    public Set<Integer> getAllStoreCard(){
        Set<Integer> storeCardIds = new HashSet<>();
        for(StoreCardId storeCardId : this.store.getIdCardsList()){
            storeCardIds.add(storeCardId.getCardId());
        }
        log.debug("All cards from store fetched");
        return storeCardIds;
    }

}
