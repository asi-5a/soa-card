package com.g2.soacard.store.servicestore;

import com.g2.soacard.common.configuration.activeMQ.ActiveMQConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;

@Import(ActiveMQConfiguration.class)
@EnableDiscoveryClient
@SpringBootApplication
public class StoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

}
