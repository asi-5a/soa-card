package model;

public enum CardIntent {

    CARDS_GENERATED, ERROR_GENERATE_CARDS, BUY_CARD, ERROR_BUY_CARD, SELL_CARD

}
