package model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CardView implements Serializable {

    private Integer idStore;

    private Integer idUser;

    private List<Integer> idCards;

}
