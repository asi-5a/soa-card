package com.g2.soacard.card.service.model;

public class CardLightModel {

    private int id;
    private float energy;
    private float hp;
    private float defence;
    private float attack;
    private float price;

    private CardReference cardReference;

    private Integer userId;
    private Integer storeId;

    public CardLightModel() {

    }

    public CardLightModel(CardModel cModel) {
        id = cModel.getId();
        this.energy=cModel.getEnergy();
        this.hp=cModel.getHp();
        this.defence=cModel.getDefence();
        this.attack=cModel.getAttack();
        this.price=cModel.getPrice();
        if( cModel.getIdUser() != null) {
            this.userId = cModel.getIdUser();
        }
        if( cModel.getIdStore()!=null) {
            this.storeId = cModel.getIdStore();
        }
        if( cModel.getCardReference()!=null) {
            this.cardReference = cModel.getCardReference();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getEnergy() {
        return energy;
    }

    public void setEnergy(float energy) {
        this.energy = energy;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getDefence() {
        return defence;
    }

    public void setDefence(float defence) {
        this.defence = defence;
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public CardReference getCardReference() {
        return cardReference;
    }

    public void setCardReference(CardReference cardReference) {
        this.cardReference = cardReference;
    }
}
