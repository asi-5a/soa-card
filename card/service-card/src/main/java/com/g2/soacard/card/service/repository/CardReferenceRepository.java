package com.g2.soacard.card.service.repository;

import com.g2.soacard.card.service.model.CardReference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardReferenceRepository extends CrudRepository<CardReference, Integer> {
}
