package com.g2.soacard.card.service.service;

import com.g2.soacard.card.service.model.CardReference;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface CardReferenceService {

    List<CardReference> getAllCardRef();

    void addCardRef(CardReference cardRef);

    CardReference getRandCardRef();

}
