package com.g2.soacard.card.service.repository;

import com.g2.soacard.card.service.model.CardModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends CrudRepository<CardModel, Integer> {

    List<CardModel> findAllByIdUser(Integer idUser);

}
