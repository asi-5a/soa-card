package com.g2.soacard.card.service.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.card.service.configuration.CardActiveMQOperation;
import com.g2.soacard.card.service.model.CardModel;
import com.g2.soacard.card.service.model.CardReference;
import com.g2.soacard.card.service.repository.CardRepository;
import com.g2.soacard.card.service.service.CardModelService;
import com.g2.soacard.card.service.service.CardReferenceService;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import lombok.extern.slf4j.Slf4j;
import model.CardIntent;
import model.StoreUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Slf4j
@Service
public class CardModelServiceImpl implements CardModelService {

	private final CardActiveMQOperation cardActiveMQOperation;

	private final CardRepository cardRepository;

	private final CardReferenceService cardReferenceService;

	private Random rand;

	@Autowired
	public CardModelServiceImpl(CardRepository cardRepository, CardReferenceService cardReferenceService, CardActiveMQOperation cardActiveMQOperation) {
		this.rand=new Random();
		this.cardActiveMQOperation = cardActiveMQOperation;
		this.cardRepository = cardRepository;
		this.cardReferenceService = cardReferenceService;
	}

	public List<CardModel> getAllCardModel() {
		Letter l = new Letter();
		l.setTopic(LinkService.USER);
		cardActiveMQOperation.sendMessage(l);
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		log.debug("All cards fetched");
		return cardList;
	}

	public void addCard(CardModel cardModel) {
		log.debug("A new card has been added : {}", cardModel.getCardReference().getName());
		cardRepository.save(cardModel);
	}

	public void updateCardRef(CardModel cardModel) {
		cardRepository.save(cardModel);

	}
	public void updateCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}
	public Optional<CardModel> getCard(Integer id) {
		log.debug("Card with id {} fetched", id);
		return cardRepository.findById(id);
	}
	
	public void deleteCardModel(Integer id) {
		log.debug("A card with the ID {} has been deleted", id);
		cardRepository.deleteById(id);
	}
	
	public List<CardModel> getRandCard(int nbr){
		List<CardModel> cardList=new ArrayList<>();
		for(int i=0;i<nbr;i++) {
			CardReference currentCardRef=cardReferenceService.getRandCardRef();
			CardModel currentCard= new CardModel();
			currentCard.setCardReference(currentCardRef);
			currentCard.setAttack(rand.nextFloat()*100);
			currentCard.setDefence(rand.nextFloat()*100);
			currentCard.setEnergy(100);
			currentCard.setHp(rand.nextFloat()*100);
			currentCard.setPrice(111);
			//save new card before sending for user creation
			this.addCard(currentCard);
			cardList.add(currentCard);
		}
		log.info("{} cards have been generated : {}", nbr, cardList.toString());
		return cardList;
	}

	public List<CardModel> getAllCardToSell(){
		log.debug("All cards for sale fetched");
		return this.cardRepository.findAllByIdUser(null);
	}

	@Override
	public void buyCard(StoreUserDto storeUserDto) {
		Optional<CardModel> cardModel = getCard(storeUserDto.getIdCard());
		if(cardModel.isPresent()){
			CardModel card = cardModel.get();

			//Si jamais la carte appartient à une autre personne : return
			if(card.getIdUser() != null)
				return;

			storeUserDto.setPriceCard(card.getPrice());

			Letter letter = new Letter();
			letter.setFrom(LinkService.CARD);
			letter.setTopic(LinkService.USER);
			letter.setIntent(CardIntent.BUY_CARD);
			letter.setMessage(storeUserDto);
			cardActiveMQOperation.sendMessage(letter);
			log.info("User with ID {} wants to buy the card {}", storeUserDto.getIdUser(), card.getCardReference().getName());
		}
	}

	@Override
	public void sellCard(StoreUserDto storeUserDto) {
		Optional<CardModel> cardModel = getCard(storeUserDto.getIdCard());
		if(cardModel.isPresent()){
			CardModel card = cardModel.get();

			//Si jamais la carte appartient à une autre personne : return
			if(!card.getIdUser().equals(storeUserDto.getIdUser()))
				return;

			storeUserDto.setPriceCard(card.getPrice());

			Letter letter = new Letter();
			letter.setFrom(LinkService.CARD);
			letter.setTopic(LinkService.USER);
			letter.setIntent(CardIntent.SELL_CARD);
			letter.setMessage(storeUserDto);
			cardActiveMQOperation.sendMessage(letter);

			log.info("User with ID {} wants to sell the card {}", storeUserDto.getIdUser(), card.getCardReference().getName());
		}
	}

	public void changeOwner(int cardId, int userId) {
		Optional<CardModel> cardModel = getCard(cardId);
		if(cardModel.isPresent()) {
			CardModel card = cardModel.get();
			card.setIdUser(userId);
			cardRepository.save(card);
			log.info("User owner of the card with name {} has been set to user with ID {}", card.getCardReference().getName(), userId);
		}
	}

	public void removeOwner(int cardId) {
		Optional<CardModel> cardModel = getCard(cardId);
		if(cardModel.isPresent()) {
			CardModel card = cardModel.get();
			card.setIdUser(null);
			cardRepository.save(card);
			log.info("User owner of the card with name {} has been removed", card.getCardReference().getName());
		}
	}
}

