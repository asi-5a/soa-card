package com.g2.soacard.card.service.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardModel{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private float energy;
	private float hp;
	private float defence;
	private float attack;
	private float price;

	private Integer idUser;

	private Integer idStore;

	@ManyToOne
	private CardReference cardReference;

	public CardModel() {}

	public CardModel(float energy, float hp,
					 float defence, float attack,float price) {
		this.energy = energy;
		this.hp = hp;
		this.defence = defence;
		this.attack = attack;
		this.price=price;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getEnergy() {
		return energy;
	}
	public void setEnergy(float energy) {
		this.energy = energy;
	}
	public float getHp() {
		return hp;
	}
	public void setHp(float hp) {
		this.hp = hp;
	}
	public float getDefence() {
		return defence;
	}
	public void setDefence(float defence) {
		this.defence = defence;
	}
	public float getAttack() {
		return attack;
	}
	public void setAttack(float attack) {
		this.attack = attack;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer userId) {
		this.idUser = userId;
	}

	public void setIdStore(Integer storeId) {
		this.idStore=storeId;
	}

	public Integer getIdStore() {
		return idStore;
	}

	public float computePrice() {
		return this.hp * 20 + this.defence*20 + this.energy*20 + this.attack*20;
	}

	public CardReference getCardReference() {
		return cardReference;
	}

	public void setCardReference(CardReference cardReference) {
		this.cardReference = cardReference;
	}
}
