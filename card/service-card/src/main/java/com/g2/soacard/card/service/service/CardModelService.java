package com.g2.soacard.card.service.service;

import com.g2.soacard.card.service.model.CardModel;
import model.StoreUserDto;
import model.view.UserCardDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CardModelService {

    List<CardModel> getAllCardModel();

    void addCard(CardModel cardModel);

    void updateCardRef(CardModel cardModel);

    void updateCard(CardModel cardModel);

    Optional<CardModel> getCard(Integer id);

    void deleteCardModel(Integer id);

    List<CardModel> getRandCard(int nbr);

    List<CardModel> getAllCardToSell();

    void buyCard(StoreUserDto storeUserDto);

    void sellCard(StoreUserDto storeUserDto);

    void changeOwner(int cardId, int userId);

    void removeOwner(int cardId);

}
