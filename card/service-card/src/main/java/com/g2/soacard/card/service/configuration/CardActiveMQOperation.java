package com.g2.soacard.card.service.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.card.service.model.CardModel;
import com.g2.soacard.card.service.service.CardModelService;
import com.g2.soacard.common.configuration.activeMQ.GeneralActiveMQOperation;
import com.g2.soacard.common.model.LinkService;
import lombok.extern.slf4j.Slf4j;
import model.CardIntent;
import com.g2.soacard.common.model.Letter;
import model.StoreIntent;
import model.StoreUserDto;
import model.UserIntent;
import model.view.CardView;
import model.view.UserCardDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class CardActiveMQOperation extends GeneralActiveMQOperation {

    private final CardModelService cardModelService;

    @Autowired
    public CardActiveMQOperation(@Lazy CardModelService cardModelService) {
        this.cardModelService = cardModelService;
    }

    @Override
    protected void receiveMessage(Letter letter) {
        LinkService from = letter.getFrom();
        switch (from){
            case USER:
                UserIntent userIntent = mapper.convertValue(letter.getIntent(), new TypeReference<UserIntent>(){});
                if(userIntent == UserIntent.GENERATE_CARDS){
                    UserCardDto cardUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<UserCardDto>(){});
                    int numberCards = cardUserDto.getNumberOfCards();
                    List<CardModel> cardModels = cardModelService.getRandCard(numberCards);

                    Letter letterToSend = new Letter();
                    letterToSend.setTopic(LinkService.USER);
                    letterToSend.setFrom(LinkService.CARD);

                    if(cardModels == null || cardModels.size() == 0){
                        letterToSend.setIntent(CardIntent.ERROR_GENERATE_CARDS);
                        letterToSend.setMessage(new CardView(null, cardUserDto.getUserId(), null));
                    }else{
                        List<Integer> idCards = new ArrayList<>();
                        for(CardModel cardModel : cardModels){
                            cardModelService.changeOwner(cardModel.getId(), cardUserDto.getUserId());
                            idCards.add(cardModel.getId());
                        }
                        letterToSend.setIntent(CardIntent.CARDS_GENERATED);
                        letterToSend.setMessage(new CardView(null, cardUserDto.getUserId(), idCards));
                    }
                    this.sendMessage(letterToSend);
                } else if(userIntent == UserIntent.SET_CARD_OWNER){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    cardModelService.changeOwner(storeUserDto.getIdCard(), storeUserDto.getIdUser());
                } else if(userIntent == UserIntent.REMOVE_CARD_OWNER){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    cardModelService.removeOwner(storeUserDto.getIdCard());
                }
                break;
            case STORE:
                StoreIntent storeIntent = mapper.convertValue(letter.getIntent(), new TypeReference<StoreIntent>(){});
                if(storeIntent == StoreIntent.BUY_CARD){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    cardModelService.buyCard(storeUserDto);
                } else if(storeIntent == StoreIntent.SELL_CARD){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    cardModelService.sellCard(storeUserDto);
                }
                break;
        }

    }

}
