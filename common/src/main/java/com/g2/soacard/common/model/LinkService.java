package com.g2.soacard.common.model;

public enum LinkService {

    CARD("CARD"),USER("USER"), STORE("STORE"), CHAT("CHAT"), CHATNODE("CHATNODE");

    private String name;

    LinkService(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
