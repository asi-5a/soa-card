package com.g2.soacard.common.configuration.activeMQ;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.model.Letter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;

@Slf4j
public abstract class GeneralActiveMQOperation {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Qualifier("objectMapper")
    @Autowired
    protected ObjectMapper mapper;

    @JmsListener(destination = "${queue.destination}", containerFactory = "connectionFactory")
    private void onMessage(Letter letter){
        log.info("New ActiveMQ message has arrived : {}", letter.toString());
        this.receiveMessage(letter);
    }

    protected abstract void receiveMessage(Letter letter);

    public void sendMessage(Letter letter) {
        try {
            if(letter == null || letter.getTopic() == null){
                    throw new Exception("No Topic set");
            }
            jmsTemplate.convertAndSend(letter.getTopic().name(), letter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
