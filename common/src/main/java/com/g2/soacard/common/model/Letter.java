package com.g2.soacard.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Letter implements Serializable {

    private String id = UUID.randomUUID().toString();

    private LinkService topic;

    private LinkService from;

    private Object intent;

    private Object message;

}
