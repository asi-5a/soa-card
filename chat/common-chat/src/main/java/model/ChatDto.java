package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ChatDto implements Serializable {

    private Integer senderId;

    private Integer receiverId;

    private String messages;

}
