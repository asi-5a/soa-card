package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public enum ChatNodeIntent implements Serializable {

    SAVE_MESSAGE("SAVE_MESSAGE"), ERROR_SAVE_MESSAGE("ERROR_SAVE_MESSAGE");

    private String chatNodeIntent;
    private static Map<String, ChatNodeIntent> mapChatNodeIntent = new HashMap<>();

    ChatNodeIntent(String chatNodeIntent){
        this.chatNodeIntent = chatNodeIntent;
    }

    public String getChatNodeIntent() {
        return chatNodeIntent;
    }

    static {
        for(ChatNodeIntent chatNodeIntentValue : ChatNodeIntent.values()){
            mapChatNodeIntent.put(chatNodeIntentValue.getChatNodeIntent(), chatNodeIntentValue);
        }
    }

    public static ChatNodeIntent fromString(String value){
        return mapChatNodeIntent.get(value);
    }

}
