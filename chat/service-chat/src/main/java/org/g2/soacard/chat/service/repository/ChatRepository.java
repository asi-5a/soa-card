package org.g2.soacard.chat.service.repository;

import org.g2.soacard.chat.service.model.Chat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRepository extends CrudRepository<Chat, Integer> {

    Optional<Chat> findBySenderIdAndReceiverId(Integer senderId, Integer receiverId);

}
