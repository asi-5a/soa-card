package org.g2.soacard.chat.service.controller;

import org.g2.soacard.chat.service.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class ChatController {

    @Autowired
    private ChatService chatService;

    @GetMapping(value = "/chat/{senderId}/{receiverId}")
    public ResponseEntity<String> getMessages(@PathVariable(value = "senderId") Integer senderId,
                                      @PathVariable(value = "receiverId") Integer receiverId){

        String messages = chatService.getMessages(senderId, receiverId);

        if(messages == null){
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(messages);

    }

}
