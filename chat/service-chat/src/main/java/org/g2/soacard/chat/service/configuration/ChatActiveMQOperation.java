package org.g2.soacard.chat.service.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.configuration.activeMQ.GeneralActiveMQOperation;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import model.ChatDto;
import model.ChatNodeIntent;
import org.g2.soacard.chat.service.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class ChatActiveMQOperation extends GeneralActiveMQOperation {

    private final ChatService chatService;

    @Autowired
    public ChatActiveMQOperation(@Lazy ChatService chatService) {
        this.chatService = chatService;
    }

    @Override
    protected void receiveMessage(Letter letter) {
        LinkService from = letter.getFrom();
        if (from == LinkService.CHATNODE) {
            ChatNodeIntent chatNodeIntent = mapper.convertValue(letter.getIntent(), new TypeReference<ChatNodeIntent>(){});
            if (chatNodeIntent == ChatNodeIntent.SAVE_MESSAGE) {
                ChatDto chatDto = mapper.convertValue(letter.getMessage(), new TypeReference<ChatDto>(){});
                chatService.saveMessage(chatDto.getSenderId(), chatDto.getReceiverId(), chatDto.getMessages());
            }
        }

    }

}
