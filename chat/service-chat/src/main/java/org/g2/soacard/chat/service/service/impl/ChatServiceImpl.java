package org.g2.soacard.chat.service.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.g2.soacard.chat.service.configuration.ChatActiveMQOperation;
import org.g2.soacard.chat.service.model.Chat;
import org.g2.soacard.chat.service.repository.ChatRepository;
import org.g2.soacard.chat.service.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ChatServiceImpl implements ChatService {

    private final ChatActiveMQOperation chatActiveMQOperation;

    private final ChatRepository chatRepository;

    @Autowired
    public ChatServiceImpl(ChatRepository chatRepository, ChatActiveMQOperation chatActiveMQOperation) {
        this.chatActiveMQOperation = chatActiveMQOperation;
        this.chatRepository = chatRepository;
    }

    @Override
    public void saveMessage(Integer senderId, Integer receiverId, String messages) {
        Chat chat = this.getChat(senderId, receiverId);
        if(chat == null){
            chat = new Chat();
            chat.setSenderId(senderId);
            chat.setReceiverId(receiverId);
        }
        chat.setMessages(messages);
        chat = chatRepository.save(chat);
        log.info("Message from chat saved : {}", chat.toString());
    }

    @Override
    public String getMessages(Integer senderId, Integer receiverId) {
        Chat chat = this.getChat(senderId, receiverId);
        if(chat == null){ return null; }
        log.debug("Messages fetched");
        return chat.getMessages();
    }

    private Chat getChat(Integer senderId, Integer receiverId){
        Optional<Chat> chatOpt = chatRepository.findBySenderIdAndReceiverId(senderId, receiverId);
        return chatOpt.orElse(null);
    }

}
