package org.g2.soacard.chat.service.service;

import org.springframework.stereotype.Service;

@Service
public interface ChatService {

    void saveMessage(Integer senderId, Integer receiverId, String messages);

    String getMessages(Integer senderId, Integer receiverId);

}
