package org.g2.soacard.user.service.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.configuration.activeMQ.GeneralActiveMQOperation;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import model.CardIntent;
import model.StoreUserDto;
import model.view.CardView;
import org.g2.soacard.user.service.service.UserModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserActiveMQOperation extends GeneralActiveMQOperation {

    private final UserModelService userModelService;

    @Autowired
    public UserActiveMQOperation(@Lazy UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    @Override
    protected void receiveMessage(Letter letter) {
        LinkService from = letter.getFrom();
        switch (from){
            case CARD:
                CardIntent cardIntent = mapper.convertValue(letter.getIntent(), new TypeReference<CardIntent>(){});
                if(cardIntent == CardIntent.CARDS_GENERATED){
                    CardView cardView = mapper.convertValue(letter.getMessage(), new TypeReference<CardView>(){});
                    int userId = cardView.getIdUser();
                    List<Integer> idCardList = cardView.getIdCards();
                    userModelService.addCardToUser(userId, idCardList);
                }else if(cardIntent == CardIntent.BUY_CARD){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    userModelService.buyCard(storeUserDto);
                }else if(cardIntent == CardIntent.SELL_CARD){
                    StoreUserDto storeUserDto = mapper.convertValue(letter.getMessage(), new TypeReference<StoreUserDto>(){});
                    userModelService.sellCard(storeUserDto);
                }
                break;
        }
    }

}

