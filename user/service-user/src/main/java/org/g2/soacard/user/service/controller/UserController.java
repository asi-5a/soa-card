package org.g2.soacard.user.service.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.log4j.Log4j2;
import model.view.UserView;
import org.g2.soacard.user.service.model.UserModel;
import org.g2.soacard.user.service.service.UserModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@Log4j2
@CrossOrigin
@RestController
public class UserController {
	
	@Autowired
	private UserModelService userModelService;
	
	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		return userModelService.getAllUsers();

	}
	
	@RequestMapping("/user/{id}")
	private UserModel getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userModelService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;

	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/user", consumes = "application/json", produces = "application/json")
	public void addUser(@RequestBody UserModel user) {
		userModelService.addUser(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public void updateUser(@RequestBody UserModel user,@PathVariable String id) {
		user.setId(Integer.valueOf(id));
		userModelService.updateUser(user);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userModelService.deleteUser(id);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/auth")
	private ResponseEntity<UserView> getAllCourses(@RequestParam("login") String login, @RequestParam("pwd") String pwd) {
		List<UserModel> userModels = userModelService.getUserByLoginPwd(login,pwd);
		if(userModels.size() > 0) {
			return ResponseEntity.ok(userModels.get(0).toUserView());
		}
		return ResponseEntity.status(401).build();
	}
	

}
