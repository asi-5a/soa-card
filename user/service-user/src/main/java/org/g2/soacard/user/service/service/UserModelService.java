package org.g2.soacard.user.service.service;

import model.StoreUserDto;
import org.g2.soacard.user.service.model.UserModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public interface UserModelService {

    public List<UserModel> getAllUsers();

    public Optional<UserModel> getUser(String id);

    public Optional<UserModel> getUser(Integer id);

    public void linkCardBuying(StoreUserDto storeUserDto);

    public void buyCard(StoreUserDto storeUserDto);

    public void sellCard(StoreUserDto storeUserDto);

    public void addUser(UserModel user);

    public void addCardToUser(int userId, List<Integer> idCardList);

    public void updateUser(UserModel user);

    public void deleteUser(String id);

    public List<UserModel> getUserByLoginPwd(String login, String pwd);

}
