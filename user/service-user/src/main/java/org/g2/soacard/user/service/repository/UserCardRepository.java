package org.g2.soacard.user.service.repository;

import org.g2.soacard.user.service.model.UserCard;
import org.springframework.data.repository.CrudRepository;

public interface UserCardRepository extends CrudRepository<UserCard, Integer> {
	
}
