package org.g2.soacard.user.service.repository;

import java.util.List;

import org.g2.soacard.user.service.model.UserModel;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<UserModel, Integer> {
	
	List<UserModel> findByLoginAndPwd(String login, String pwd);

}
