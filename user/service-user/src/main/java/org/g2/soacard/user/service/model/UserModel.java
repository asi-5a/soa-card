package org.g2.soacard.user.service.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import model.view.UserView;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String login;
	private String pwd;
	private float account;
	private String lastName;
	private String surName;
	private String email;

	@OneToMany(fetch = FetchType.EAGER)
	private List<UserCard> cardList = new ArrayList<>();

	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public List<UserCard> getCardList() {
		return cardList;
	}

	public void setCardList(List<UserCard> cardList) {
		this.cardList = cardList;
	}

	public void addAllCardList(Collection<UserCard> cardList) {
		this.cardList.addAll(cardList);
	}


	public void addCard(UserCard card) {
		this.cardList.add(card);
	}

	public boolean checkIfCard(int idCard){
		for(UserCard c_c: this.cardList){
			if(c_c.getIdCard() == idCard){
				return true;
			}
		}
		return false;
	}

	public boolean removeCardById(int idCard) {
		for(UserCard c_c: this.cardList){
			if(c_c.getIdCard() == idCard){
				this.cardList.remove(c_c);
				return true;
			}
		}
		return false;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserView toUserView(){
		List<Integer> cardIds = new ArrayList<>();
		for(UserCard userCard : this.cardList){
			cardIds.add(userCard.getIdCard());
		}
		return new UserView(this.id, this.login, this.account, this.lastName, this.surName, this.email, cardIds);
	}

}
