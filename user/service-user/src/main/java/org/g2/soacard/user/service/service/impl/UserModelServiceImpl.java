package org.g2.soacard.user.service.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.g2.soacard.common.model.Letter;
import com.g2.soacard.common.model.LinkService;
import model.StoreUserDto;
import model.UserIntent;
import model.view.UserCardDto;
import org.g2.soacard.user.service.configuration.UserActiveMQOperation;
import org.g2.soacard.user.service.model.UserCard;
import lombok.extern.slf4j.Slf4j;
import org.g2.soacard.user.service.model.UserModel;
import org.g2.soacard.user.service.repository.UserCardRepository;
import org.g2.soacard.user.service.repository.UserRepository;
import org.g2.soacard.user.service.service.UserModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class UserModelServiceImpl implements UserModelService {

	private final UserRepository userRepository;
	private final UserActiveMQOperation userActiveMQOperation;
	private final UserCardRepository userCardRepository;

	@Autowired
	public UserModelServiceImpl(UserRepository userRepository, UserCardRepository userCardRepository, UserActiveMQOperation userActiveMQOperation) {
		this.userRepository = userRepository;
		this.userActiveMQOperation = userActiveMQOperation;
		this.userCardRepository = userCardRepository;
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		log.debug("All users fetched");
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		log.debug("User with id {} fetched", id);
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		log.debug("User with id {} fetched", id);
		return userRepository.findById(id);
	}

	@Override
	public void linkCardBuying(StoreUserDto storeUserDto) {
		Optional<UserModel> userModel = getUser(storeUserDto.getIdUser());
		if(userModel.isPresent()){
			Letter letter = new Letter();
			letter.setFrom(LinkService.USER);
			letter.setTopic(LinkService.CARD);
			letter.setIntent(UserIntent.BUY_CARD);
			letter.setMessage(new StoreUserDto(storeUserDto.getIdUser(), storeUserDto.getIdCard(), storeUserDto.getPriceCard()));
			userActiveMQOperation.sendMessage(letter);
			log.debug("User {} is buying card {} --> Asking card service", userModel.get().getLogin(), storeUserDto.getIdCard());
		}
	}

	@Override
	public void buyCard(StoreUserDto storeUserDto) {
		Optional<UserModel> userModel = getUser(storeUserDto.getIdUser());
		if(userModel.isPresent()) {
			UserModel user = userModel.get();
			if(user.getAccount() > storeUserDto.getPriceCard()) {
				UserCard userCard = new UserCard(storeUserDto.getIdCard());
				userCardRepository.save(userCard);
				user.addCard(userCard);
				user.setAccount(user.getAccount() - storeUserDto.getPriceCard());
				updateUser(user);

				//Changement de proprio sur la carte
				Letter letter = new Letter();
				letter.setFrom(LinkService.USER);
				letter.setTopic(LinkService.CARD);
				letter.setIntent(UserIntent.SET_CARD_OWNER);
				letter.setMessage(storeUserDto);
				userActiveMQOperation.sendMessage(letter);

				log.info("User {} has bought card {}", user.getLogin(), storeUserDto.getIdCard());
			}
		}
	}

	@Override
	public void sellCard(StoreUserDto storeUserDto) {
		Optional<UserModel> userModel = getUser(storeUserDto.getIdUser());
		if(userModel.isPresent()) {
			UserModel user = userModel.get();
			if(user.checkIfCard(storeUserDto.getIdCard())) { //Vérifie que l'utilisateur possède la carte
				user.removeCardById(storeUserDto.getIdCard());

				user.setAccount(user.getAccount() + storeUserDto.getPriceCard());
				updateUser(user);

				//Changement de proprio sur la carte
				Letter letter = new Letter();
				letter.setFrom(LinkService.USER);
				letter.setTopic(LinkService.CARD);
				letter.setIntent(UserIntent.REMOVE_CARD_OWNER);
				letter.setMessage(storeUserDto);
				userActiveMQOperation.sendMessage(letter);

				log.info("User {} has sold card {}", user.getLogin(), storeUserDto.getIdCard());
			}
		}
	}

	public void addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		user = userRepository.save(user);

		Letter letterToSend = new Letter(); //Envoi de la requette de génération de carte au service card
		letterToSend.setTopic(LinkService.CARD);
		letterToSend.setFrom(LinkService.USER);
		letterToSend.setIntent(UserIntent.GENERATE_CARDS);
		letterToSend.setMessage(new UserCardDto(5, user.getId()));
		userActiveMQOperation.sendMessage(letterToSend);
		log.info("A new user has been added : {}", user.getLogin());
	}

	public void addCardToUser(int userId, List<Integer> idCardList) {
		Optional<UserModel> userOptional = this.getUser(userId);
		if(userOptional.isPresent()) {
			UserModel user = userOptional.get();
			UserCard userCard = null;
			for(int id : idCardList) {
				userCard = new UserCard(id);
				userCardRepository.save(userCard);
				user.addCard(userCard);
			}
			userRepository.save(user);
			log.info("User {} has now cards {}", user.getLogin(), idCardList.toString());
		}
		//TODO: gérer les erreurs dans le else
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}

}
