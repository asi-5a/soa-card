package model.view;

import java.io.Serializable;

public class UserCardDto implements Serializable {

    private int numberOfCards;
    private int userId;

    public UserCardDto() {
    }

    public UserCardDto(int numberOfCards, int userId) {
        this.numberOfCards = numberOfCards;
        this.userId = userId;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }

    public void setNumberOfCards(int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
