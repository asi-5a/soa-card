package model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserView {

    private Integer id;

    private String login;

    private float account;

    private String lastName;

    private String surName;

    private String eMail;

    private List<Integer> idCards;

}
