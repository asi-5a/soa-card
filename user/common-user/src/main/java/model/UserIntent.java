package model;

public enum UserIntent {
    BUY_CARD, ERROR_BUY_CARD, GENERATE_CARDS, ERROR_BUY_CARDS, SET_CARD_OWNER, REMOVE_CARD_OWNER
}
